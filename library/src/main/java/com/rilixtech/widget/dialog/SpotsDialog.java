package com.rilixtech.widget.dialog;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import java.lang.ref.WeakReference;

/**
 * Updated by joielechong | juansspy@gmail.com
 * 0n 20 october 2019
 * Created by Maxim Dybarsky | maxim.dybarskyy@gmail.com
 * on 13.01.15 at 14:22
 */
public class SpotsDialog extends AlertDialog {

  public static class Builder {

    private WeakReference<Context> contextRef;
    private String message;
    private int messageId;
    private int themeId;
    private boolean cancelable = true; // default dialog behaviour
    private boolean canceledOnTouchOutside = false; // default dialog behaviour
    private OnCancelListener cancelListener;

    public Builder setContext(Context context) {
      this.contextRef = new WeakReference<>(context);
      return this;
    }

    public Builder setMessage(String message) {
      this.message = message;
      return this;
    }

    /**
     * Set message by String resource id.
     *
     * @param messageId id of string in strings resource.
     * @return Builder of the SpotsDialog
     */
    public Builder setMessage(int messageId) {
      this.messageId = messageId;
      return this;
    }

    /**
     * Set theme by the style resource reference (e.g. {@code android.R.style.TextAppearance}).
     *
     * @param themeId id of the theme in resource file.
     * @return Builder of the SpotsDialog
     */
    public Builder setTheme(int themeId) {
      this.themeId = themeId;
      return this;
    }

    public Builder setCancelable(boolean cancelable) {
      this.cancelable = cancelable;
      return this;
    }

    public Builder setCanceledOnTouchOutside(boolean canceledOnTouchOutside) {
      this.canceledOnTouchOutside = canceledOnTouchOutside;
      return this;
    }

    public Builder setCancelListener(OnCancelListener cancelListener) {
      this.cancelListener = cancelListener;
      return this;
    }

    public AlertDialog build() {
      Context context = contextRef.get();
      return new SpotsDialog(
          context,
          messageId != 0 ? context.getString(messageId) : message,
          themeId != 0 ? themeId : R.style.SpotsDialogDefault,
          cancelable,
          canceledOnTouchOutside,
          cancelListener
      );
    }
  }

  private static final int DELAY = 150;
  private static final int DURATION = 1500;

  private int size;
  private AnimatedView[] spots;
  private AnimatorPlayer animator;
  private CharSequence message;

  private SpotsDialog(Context context, String message, int theme, boolean cancelable,
      boolean CanceledOnTouchOutside, OnCancelListener listener) {

    super(context, theme);
    this.message = message;

    setCancelable(cancelable);
    setCanceledOnTouchOutside(CanceledOnTouchOutside);
    if (listener != null) setOnCancelListener(listener);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.dmax_spots_dialog);

    initMessage();
    Context context = getContext();
    initProgress(context);
  }

  @Override
  protected void onStart() {
    super.onStart();

    for (AnimatedView view : spots) view.setVisibility(View.VISIBLE);

    animator = new AnimatorPlayer(createAnimations());
    animator.play();
  }

  @Override
  protected void onStop() {
    animator.stop();
    super.onStop();
  }

  @Override public void dismiss() {
    animator.stop();
    spots = null;
    super.dismiss();
  }

  @Override
  public void setMessage(CharSequence message) {
    this.message = message;
    if (isShowing()) initMessage();
  }

  private void initMessage() {
    if (message != null && message.length() > 0) {
      ((TextView) findViewById(R.id.dmax_spots_title)).setText(message);
    }
  }

  private void initProgress(Context context) {
    ProgressLayout progress = findViewById(R.id.dmax_spots_progress);
    size = progress.getSpotsCount();

    spots = new AnimatedView[size];
    int size = context.getResources().getDimensionPixelSize(R.dimen.spot_size);
    int progressWidth = context.getResources().getDimensionPixelSize(R.dimen.progress_width);
    for (int i = 0; i < spots.length; i++) {
      AnimatedView v = new AnimatedView(context);
      v.setBackgroundResource(R.drawable.dmax_spots_spot);
      v.setTarget(progressWidth);
      v.setXFactor(-1f);
      v.setVisibility(View.INVISIBLE);
      progress.addView(v, size, size);
      spots[i] = v;
    }
  }

  private Animator[] createAnimations() {
    Animator[] animators = new Animator[size];
    for (int i = 0; i < spots.length; i++) {
      final AnimatedView animatedView = spots[i];
      Animator move = ObjectAnimator.ofFloat(animatedView, "xFactor", 0, 1);
      move.setDuration(DURATION);
      move.setInterpolator(new HesitateInterpolator());
      move.setStartDelay(DELAY * i);
      move.addListener(new AnimatorListenerAdapter() {
        @Override
        public void onAnimationEnd(Animator animation) {
          animatedView.setVisibility(View.INVISIBLE);
        }

        @Override
        public void onAnimationStart(Animator animation) {
          animatedView.setVisibility(View.VISIBLE);
        }
      });
      animators[i] = move;
    }
    return animators;
  }
}
